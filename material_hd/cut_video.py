from errno import EEXIST
from itertools import count
from os import makedirs
from os.path import join

from moviepy.editor import VideoFileClip
from moviepy.video.fx.all import blackwhite, crop

dirs_root = ["faces", "partials", "hands", "wtfs"]
dirs_sub = ["momente", "mikrophonie", "jeita"]

for root in dirs_root:
    for sub in dirs_sub:
        name = join(root, sub)
        try:
            makedirs(name)
        except OSError as e:
            if e.errno != EEXIST:
                raise


def cut_scenes(src, scenes, dest):
    file_ext = ".avi"
    codec = 'png'
    width = 640
    height = 480
    for scene, i in zip(scenes, count(0)):
        tag = ""
        if len(scene) > 2:
            tag = scene[2]
        video = VideoFileClip(src).subclip(scene[0], scene[1])

        widthStart = (video.w - width) // 2
        heightStart = (video.h - height) // 2
        video = crop(video, x1=widthStart, y1=heightStart,
                     width=width, height=height)
        video = blackwhite(video)
        name = join(dest, tag + str(i) + file_ext)
        video.write_videofile(name, fps=25, codec=codec,
                              audio=False, preset='placebo')


# tags
drum_tag = "drum"
god_tag = "god"
real_tag = "real"
mikro_tag = "mikro"
mixer_tag = "mixer"
radio_tag = "radio"
stuff_tag = "stuff"
switch_tag = "switch"


momente_faces = [((2, 38), (2, 58)),
                 ((3, 0), (3, 3)),
                 ((3, 5), (3, 16)),
                 ((3, 21), (3, 33)),
                 ((3, 36), (3, 41)),
                 ((3, 44), (3, 49)),
                 ((5, 10), (5, 32)),
                 ((27, 51), (28, 10)),
                 ((28, 14), (28, 45)),
                 ((30, 3), (30, 26)),
                 ((30, 35), (30, 51)),
                 ((32, 8), (32, 18))]

momente_partials = [((13, 17), (13, 19)),
                    ((28, 46), (28, 48)),
                    ((29, 31), (29, 37)),
                    ((29, 40), (29, 45)),
                    ((32, 0), (32, 8)),
                    ((39, 20), (39, 22))]

momente_hands = [((9, 15), (9, 17), god_tag),
                 ((9, 25), (9, 26), god_tag),
                 ((10, 36), (10, 41), real_tag),
                 ((15, 54), (15, 57), real_tag),
                 ((19, 50), (19, 53), drum_tag),
                 ((27, 14), (27, 17)),
                 ((27, 49), (27, 50))]

momente_wtfs = [((23, 37), (23, 41)),
                ((41, 36), (41, 38))]

cut_scenes("momente.mkv", momente_faces, join(dirs_root[0], dirs_sub[0]))
cut_scenes("momente.mkv", momente_partials, join(dirs_root[1], dirs_sub[0]))
cut_scenes("momente.mkv", momente_hands, join(dirs_root[2], dirs_sub[0]))
cut_scenes("momente.mkv", momente_wtfs, join(dirs_root[3], dirs_sub[0]))

mikro_faces = [((1, 26), (1, 29)),
               ((1, 32), (1, 34)),
               ((10, 39), (10, 40)),
               ((11, 17), (11, 23)),
               ((11, 25), (11, 27)),
               ((13, 48), (13, 51)),
               ((13, 55), (14, 0)),
               ((16, 39), (16, 42)),
               ((16, 58), (17, 1)),
               ((17, 6), (17, 8)),
               ((17, 20), (17, 21)),
               ((17, 27), (17, 31)),
               ((17, 50), (17, 51)),
               ((18, 22), (18, 23)),
               ((18, 25), (18, 26)),
               ((18, 29), (18, 30)),
               ((19, 7), (19, 9))]

mikro_hands = [((4, 44), (4, 47), mikro_tag),
               ((4, 50), (4, 56), mikro_tag),
               ((7, 39), (7, 42), switch_tag),
               ((9, 15), (9, 19), mikro_tag),
               ((10, 10), (10, 11), switch_tag),
               ((19, 12), (19, 13)),
               # ((19, 18), (19, 19.5)),  blurred fadeout..
               ((19, 51), (19, 53), drum_tag),
               ((20, 39), (20, 40)),
               ((20, 45), (20, 46)),
               ((21, 30), (21, 31), drum_tag),
               ((24, 40), (24, 49), mikro_tag)]

cut_scenes("mikrophonie.mkv", mikro_faces, join(dirs_root[0], dirs_sub[1]))
cut_scenes("mikrophonie.mkv", mikro_hands, join(dirs_root[2], dirs_sub[1]))

jeita_partials = [((8, 3), (8, 4))]

jeita_hands = [((10, 28), (10, 29), stuff_tag),
               ((10, 50), (10, 51), stuff_tag),
               ((12, 25), (12, 26), radio_tag),
               ((16, 38), (16, 40), switch_tag),
               ((18, 9), (18, 16), radio_tag),
               ((27, 34), (27, 36), stuff_tag),
               ((36, 30), (36, 31), mixer_tag),
               ((36, 50), (36, 53), mixer_tag),
               ((40, 8), (40, 9), mixer_tag)]

jeita_wtfs = [((4, 32), (4, 34)),
              ((23, 14), (23, 27)),
              ((32, 50), (32, 53))]

cut_scenes("jeita.mkv", jeita_partials, join(dirs_root[1], dirs_sub[2]))
cut_scenes("jeita.mkv", jeita_hands, join(dirs_root[2], dirs_sub[2]))
cut_scenes("jeita.mkv", jeita_wtfs, join(dirs_root[3], dirs_sub[2]))
