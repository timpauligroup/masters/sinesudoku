sr = 96000
ksmps = 1
nchnls = 2
0dbfs = 729

alwayson "sudokureverb"
connect "sudokupointsine","out","sudokureverb","in"
connect "sudokulinesine","out","sudokureverb","in"

// globals.. because of performance reasons
gisine ftgen 0, 0, 2^9, 10, 1

// opcode for creating an envelope for glissandi
opcode glis, k, ii
    iAim, iDur xin
    iSteep = 4.5
    if iAim < 1 then
      iAttSteepness = -iSteep
    else
      iAttSteepness = iSteep
    endif
    kGlis transeg 1, iDur, iAttSteepness, iAim
    xout kGlis
endop

// opcode for creating an envelope for fading the sounds in and out.
opcode fade, k, iii
  iAttack, iDuration, iRelease xin
  iSmallestAmp init 0
  iAttSteepness init 4.5
  iRelSteepness init -iAttSteepness
  if iAttack <= 0 && iRelease <= 0 then
    kfade = 1
  elseif iAttack > 0 && iRelease > 0 && iAttack + iRelease == iDuration then
    kfade transeg iSmallestAmp, iAttack, iAttSteepness, 1, iRelease, iRelSteepness, iSmallestAmp
  elseif iRelease <= 0 && iAttack == iDuration then
    kfade transeg iSmallestAmp, iAttack, iAttSteepness, 1
  elseif iAttack <= 0 && iRelease == iDuration then
    kfade transeg 1, iRelease, iRelSteepness, iSmallestAmp
  elseif iRelease <= 0 && iAttack < iDuration then
    kfade transeg iSmallestAmp, iAttack, iAttSteepness, 1, iDuration - iAttack, 0, 1
  elseif iAttack <= 0  && iRelease < iDuration then
    kfade transeg 1, iDuration - iRelease, 0, 1, iRelease, iRelSteepness, iSmallestAmp
  else
    kfade transeg iSmallestAmp, iAttack, iAttSteepness, 1, iDuration - iAttack - iRelease, 0, 1, iRelease, iRelSteepness, iSmallestAmp
  endif
  xout kfade
endop

// opcode for scaling the point duration according to length, but that it does not get longer than a certain max.
opcode scale, i, ii
  iDur, iMax xin
  if iDur < 1 then
    iScaled = iDur * iMax
  else
    iScaled = (1/iDur) * iMax
  endif
  xout iScaled
endop

// opcode for determining the right length of the points. so that there are not longer then the duration.
opcode point, iii, i
  iDuration xin
  iRatio init 1/(9*9*9)
  iPointInit init 1/(9*9)
  iPointDuration scale iDuration, iPointInit
  iJump = iRatio * iPointDuration
  if iDuration > (iPointDuration + iJump) then
    iSilence = iDuration - iPointDuration - iJump
  else
    iSilence = iRatio * iDuration
    iJump = iRatio * iDuration
    iPointDuration = iDuration - iJump - iSilence
  endif
  xout iPointDuration, iSilence, iJump
endop

;subinstruments for playing one partial

instr sudokupointsine
  ;clarifying arguments (p-fields)
  iduration = p3
  iphase = p4
  iattack = p5
  irelease = p6
  ifreq_repitions = p7
  iamp_repitions = p8
  iGlisAim = p45

  // envelopes
  kfreq_envelope lpshold  ifreq_repitions, 0, iphase,
                          p9, p18,
                          p10,p19,
                          p11,p20,
                          p12,p21,
                          p13,p22,
                          p14,p23,
                          p15,p24,
                          p16,p25,
                          p17,p26

  iPointDur1, iSilence1, iJump1 point p36
  iPointDur2, iSilence2, iJump2 point p37
  iPointDur3, iSilence3, iJump3 point p38
  iPointDur4, iSilence4, iJump4 point p39
  iPointDur5, iSilence5, iJump5 point p40
  iPointDur6, iSilence6, iJump6 point p41
  iPointDur7, iSilence7, iJump7 point p42
  iPointDur8, iSilence8, iJump8 point p43
  iPointDur9, iSilence9, iJump9 point p44

  kamp_envelope loopseg   iamp_repitions, 0, iphase,
                          p27,iPointDur1,0,iSilence1,0,iJump1,
                          p28,iPointDur2,0,iSilence2,0,iJump2,
                          p29,iPointDur3,0,iSilence3,0,iJump3,
                          p30,iPointDur4,0,iSilence4,0,iJump4,
                          p31,iPointDur5,0,iSilence5,0,iJump5,
                          p32,iPointDur6,0,iSilence6,0,iJump6,
                          p33,iPointDur7,0,iSilence7,0,iJump7,
                          p34,iPointDur8,0,iSilence8,0,iJump8,
                          p35,iPointDur9,0,iSilence9,0,iJump9

  kfade fade iattack, iduration, irelease
  kGlis glis iGlisAim, iduration

  ; synthesis
  asine oscili kamp_envelope * kfade, kfreq_envelope * kGlis, gisine, iphase

  // output
  outleta "out", asine
  outs asine, asine
endin

instr sudokulinesine
  ;clarifying arguments (p-fields)
  iduration = p3
  iphase = p4
  iattack = p5
  irelease = p6
  ifreq_repitions = p7
  iamp_repitions = p8
  iGlisAim = p45

  // envelopes
  kfreq_envelope loopseg  ifreq_repitions, 0, iphase,
                          p9, p18,
                          p10,p19,
                          p11,p20,
                          p12,p21,
                          p13,p22,
                          p14,p23,
                          p15,p24,
                          p16,p25,
                          p17,p26

  kamp_envelope loopseg   iamp_repitions, 0, iphase,
                          p27,p36,
                          p28,p37,
                          p29,p38,
                          p30,p39,
                          p31,p40,
                          p32,p41,
                          p33,p42,
                          p34,p43,
                          p35,p44

  kfade fade iattack, iduration, irelease
  kGlis glis iGlisAim, iduration

  ; synthesis
  asine oscili kamp_envelope * kfade, kfreq_envelope * kGlis, gisine, iphase

  // output
  outleta "out", asine
  outs asine, asine
endin

instr sudokureverb
  // arguments
  iFeedback init 0.9
  iHiCut init 9*9*9*9
  iAmp init 1/9

  aIn inleta "in"

  // synthesis
  aReverbLeft, aReverbRight reverbsc aIn*iAmp, aIn*iAmp, iFeedback, iHiCut

  // output
  outs aReverbLeft, aReverbRight
endin

instr silence
  aSilence = 0
  outs aSilence, aSilence
endin
