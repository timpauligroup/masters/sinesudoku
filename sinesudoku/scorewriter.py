def write_single(f, value):
    f.write('{0:f}'.format(round(value, 6)) + " ")


def write_string(f, value):
    f.write(value + " ")


def write_envelope(f, envelope):
    f.write("/**/ ")
    envelope = map(lambda x: round(x, 6), envelope)
    envelope = map(str, envelope)
    f.write(' '.join(envelope))
    f.write(" ")


def write_eol(f):
    f.write("\n")
