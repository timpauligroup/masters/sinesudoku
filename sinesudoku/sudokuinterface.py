def check_aliasing(x):
    return x > 22050 / 81


def create_scale(max_octave):
    scale = []
    fundamental = 17
    ratios = [1, float(17) / 16, float(9) / 8, float(5) / 4, float(11) / 8,
              float(3) / 2, float(13) / 8, float(7) / 4, float(15) / 8]
    for octave in range(max_octave):
        for ratio in ratios:
            freq = fundamental * (2**octave) * ratio
            if not check_aliasing(freq):
                scale.append(freq)
            else:
                raise ValueError("octave: " + str(octave) +
                                 " freq: " + str(freq) +
                                 "ALIASING with overtones")
    scale.append(fundamental * (2**max_octave))
    return scale


scale = create_scale(4)


def create_ratios():
    ratios = [1 / 81]
    for i in range(1, 10):
        ratios.append(i / 9)
    return ratios


ratios = create_ratios()

if __name__ == '__main__':
    print(scale)
    print(ratios)
