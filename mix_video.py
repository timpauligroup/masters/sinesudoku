from moviepy.editor import VideoFileClip

from config import name, sec, video_secs
# TODO local fix 2
from sinesudoku.concatenate_debug import concatenate_videoclips
from sinesudoku.sudokuvideo import Sudokuvideosizer

# open
print("open")
directory = Sudokuvideosizer.build_dir
suffix = ".avi"
clips = [VideoFileClip(directory + sec + str(s) + suffix) for s in video_secs]

print("concatenate")
final_clip = concatenate_videoclips(clips)

print("export")
end_suf = '.mov'
videofilename = directory + name + end_suf
final_clip.write_videofile(videofilename, codec='libx264', preset='veryslow',
                           ffmpeg_params=['-crf', '0'])
