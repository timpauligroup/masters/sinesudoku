import unittest

from sinesudoku import sudokuinterface


class SudokuinterfaceTests(unittest.TestCase):

    def testScale(self):
        result = sudokuinterface.create_scale(1)
        correct_answere = [17, 18.0625, 19.125, 21.25,
                           23.375, 25.5, 27.625, 29.75, 31.875, 34]
        self.failUnless(result == correct_answere)

    def testRatios(self):
        result = sudokuinterface.create_ratios()
        correct_answere = [1 / 81,
                           1 / 9, 2 / 9, 3 / 9, 4 / 9,
                           5 / 9, 6 / 9, 7 / 9, 8 / 9,
                           1]
        self.failUnless(result == correct_answere)


if __name__ == '__main__':

    unittest.main()
