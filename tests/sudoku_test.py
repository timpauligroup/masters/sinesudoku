import unittest

from sinesudoku import sudoku


class SudokuTests(unittest.TestCase):

    def setUp(self):
        self.size = 9
        self.s = sudoku.Sudoku(testempty2d(), self.size)

    def testRows(self):
        for row, rowO in zip(self.s.rows, testempty2d()):
            self.failUnless([c.value for c in row] == rowO)

    def testRowIndex(self):
        for row, i in zip(self.s.rows, range(9)):
            self.failUnless(row.index == i)

    def testColumnPerspective(self):
        for row, row0 in zip(self.s.rows, testempty2d()):
            self.failUnless([c.value for c in row.get(
                0).getRowPerspective()] == row0)

    def testCellColumnIndex(self):
        for column, i in zip(self.s.columns, range(9)):
            for cell in column:
                self.failUnless(cell.columnIndex == i)

    def testClosedNetwork(self):
        for box in self.s.boxes:
            for cell in box:
                self.failUnless(cell._box is box)
        for row in self.s.rows:
            for cell in row:
                self.failUnless(cell._row is row)
        for column in self.s.columns:
            for cell in column:
                self.failUnless(cell._column is column)

    def testNormalize(self):
        first = self.s.rows[0][0].normalize()
        self.failUnless(first == 5 / 9)
        second = self.s.rows[1][0].normalize()
        self.failUnless(second == 0)

    def testGetXandYfromBoxSpecial(self):
        x, y = self.s.getXandYfromBox(7, 5)
        self.failUnless(x == 5)
        self.failUnless(y == 7)

    def testGetXandYfromBoxBasic(self):
        x, y = self.s.getXandYfromBox(0, 0)
        self.failUnless(x == 0)
        self.failUnless(y == 0)
        x, y = self.s.getXandYfromBox(8, 8)
        self.failUnless(x == 8)
        self.failUnless(y == 8)


if __name__ == '__main__':

    unittest.main()


def testempty2d():
    return [
        [5, 3, 4, 6, 7, 8, 9, 1, 2],
        [0, 0, 0, 1, 9, 5, 0, 0, 0],
        [0, 0, 8, 0, 0, 0, 0, 6, 0],
        [8, 0, 0, 0, 6, 0, 0, 0, 0],
        [4, 0, 0, 8, 0, 0, 0, 0, 1],
        [0, 0, 0, 0, 2, 0, 0, 0, 0],
        [0, 6, 0, 0, 0, 0, 2, 8, 0],
        [0, 0, 0, 4, 1, 9, 0, 0, 5],
        [0, 0, 0, 0, 0, 0, 0, 7, 0]
    ]


def testfull2d():
    return [
        [5, 3, 4, 6, 7, 8, 9, 1, 2],
        [6, 7, 2, 1, 9, 5, 3, 4, 8],
        [1, 9, 8, 3, 4, 2, 5, 6, 7],
        [8, 5, 9, 7, 6, 1, 4, 2, 3],
        [4, 2, 6, 8, 5, 3, 7, 9, 1],
        [7, 1, 3, 9, 2, 4, 8, 5, 6],
        [9, 6, 1, 5, 3, 7, 2, 8, 4],
        [2, 8, 7, 4, 1, 9, 6, 3, 5],
        [3, 4, 5, 2, 8, 6, 1, 7, 9]
    ]
