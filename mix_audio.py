from pydub import AudioSegment

from config import audio_secs, name, sec
from sinesudoku.sudokusynth import Sudokusynth


def post_process(audio, headroom, threshold):
    output = audio.low_pass_filter(20000)
    output = output.high_pass_filter(20)
    output = output.normalize(0)
    output = output.compress_dynamic_range(threshold, 9, 1, 9)
    output = output.normalize(headroom)
    return output


# open
print("open")
directory = Sudokusynth.build_dir
suf = ".wav"
audios = [AudioSegment.from_file(
    directory + sec + str(s) + suf, format='wav') for s in audio_secs]
assert(len(audios) == 9)

# mix
print("mix")
output = AudioSegment.silent(duration=len(audios[0]))
assert(all([len(a) == len(audios[0]) for a in audios]))
thresholds = [-9, -9, -9, -9, -9, -27, -9, -9, -18]
headrooms = [3, 2, 1, 3, 2, 1, 3, 3, 0]
assert(len(thresholds) == len(headrooms))

for a, headroom, threshold in zip(audios, headrooms, thresholds):
    print("mix: post_process")
    b = post_process(a, headroom, threshold)
    print("mix: overlay")
    output = output.overlay(b)

# post
print("post")
output = output.normalize(0.3)

# export
print("export")
output.export(directory + name + suf, format="wav")
