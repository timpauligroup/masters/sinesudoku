import itertools

from sinesudoku.sudoku import Sudoku
from sinesudoku.sudokuinterface import ratios, scale
from sinesudoku.sudokusynth import Synthtype


def score1(synth, time, max_amp, soundon, duration2):
    """
        FLIPPING THE PAGE - LINES
        inspired by a visual scene where someone browses through a sudoku book.
        simultaneous fadein and fadeout for flipping the page.
        the last sound should fade out during the entire next section.
    """
    print("SECTION 1")
    # sudoku setup
    sudokuraw = []

    sudokuraw.append([
        [4, 9, 2, 0, 0, 0, 7, 5, 6],
        [0, 0, 0, 0, 0, 4, 8, 0, 0],
        [0, 3, 8, 0, 0, 0, 4, 2, 0],
        [6, 0, 0, 1, 8, 0, 5, 0, 0],
        [0, 0, 0, 0, 6, 0, 0, 0, 0],
        [0, 0, 7, 0, 9, 5, 0, 0, 1],
        [0, 6, 1, 0, 0, 0, 2, 7, 0],
        [0, 0, 3, 8, 0, 0, 0, 0, 0],
        [2, 4, 5, 0, 0, 0, 9, 3, 8]])

    sudokuraw.append([
        [3, 0, 0, 0, 2, 9, 0, 0, 7],
        [0, 4, 0, 0, 1, 0, 0, 2, 3],
        [0, 0, 0, 0, 0, 3, 9, 0, 5],
        [0, 0, 3, 0, 8, 0, 0, 0, 9],
        [0, 2, 8, 3, 0, 4, 5, 6, 0],
        [5, 0, 0, 0, 6, 0, 2, 0, 0],
        [8, 0, 5, 7, 0, 0, 0, 0, 0],
        [2, 7, 0, 0, 5, 0, 0, 8, 0],
        [4, 0, 0, 9, 3, 0, 0, 0, 2]])

    sudokuraw.append([
        [0, 9, 7, 0, 0, 1, 5, 0, 0],
        [5, 0, 0, 6, 7, 0, 8, 0, 0],
        [0, 0, 0, 0, 4, 0, 0, 3, 0],
        [0, 5, 0, 0, 0, 0, 2, 0, 0],
        [8, 3, 0, 0, 0, 0, 0, 5, 7],
        [0, 0, 6, 0, 0, 0, 0, 1, 0],
        [0, 2, 0, 0, 9, 0, 0, 0, 0],
        [0, 0, 5, 0, 1, 4, 0, 0, 8],
        [0, 0, 1, 3, 0, 0, 9, 2, 0]])

    sudokuraw.append([
        [0, 1, 7, 2, 0, 0, 0, 0, 0],
        [0, 9, 2, 3, 7, 8, 0, 0, 5],
        [4, 8, 0, 6, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 6, 9, 2],
        [0, 0, 0, 0, 2, 0, 0, 0, 0],
        [2, 7, 4, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 3, 0, 6, 7],
        [1, 0, 0, 8, 5, 7, 9, 2, 0],
        [0, 0, 0, 0, 0, 2, 5, 3, 0]])

    sudokuraw.append([
        [0, 8, 1, 7, 0, 4, 0, 0, 0],
        [7, 0, 0, 0, 0, 0, 0, 0, 3],
        [0, 0, 0, 5, 2, 0, 0, 9, 0],
        [4, 1, 3, 0, 0, 0, 0, 0, 0],
        [0, 0, 9, 0, 0, 0, 8, 0, 0],
        [0, 0, 0, 0, 0, 0, 3, 6, 9],
        [0, 7, 0, 0, 8, 5, 0, 0, 0],
        [5, 0, 0, 0, 0, 0, 0, 0, 8],
        [0, 0, 0, 1, 0, 9, 7, 5, 0]])

    sudoku = []
    for raw in sudokuraw:
        sudoku.append(Sudoku(raw, 9))

    currenttime = time
    row = sudoku[0].rows[0]
    box = sudoku[0].boxes[0]
    column = sudoku[0].columns[0]

    # column foroc because we want a deep groan at the end
    foroc = map(lambda x: 9 * int(x) // 3, column)
    forin = map(lambda x: int(x), box)
    amps = itertools.chain([max_amp - i for i in reversed(range(9))])
    glisses = map(lambda x: ratios[int(x)], row)

    def pitch():
        return scale[next(foroc) + next(forin)]

    index = 0
    timeunit = 4

    def play(sudokunumber, currenttime, d, a, r):
        s = sudoku[sudokunumber]
        glis = next(glisses)
        amp = next(amps)
        freq = pitch()
        if soundon:
            assert amp <= max_amp
            synth.synthesize_sudoku(currenttime,
                                    s,
                                    Synthtype.LINE,
                                    d, a, r,
                                    freq, glis, amp)
        return index

    index = play(0, currenttime, 4 * timeunit, timeunit, timeunit)
    currenttime += 3 * timeunit

    index = play(2, currenttime, 4 * timeunit, timeunit, timeunit)
    currenttime += timeunit
    index = play(1, currenttime, 4 * timeunit, timeunit, timeunit)
    currenttime += 2 * timeunit

    index = play(4, currenttime, 5 * timeunit, timeunit, timeunit)
    currenttime += timeunit
    index = play(3, currenttime, 3 * timeunit, timeunit, timeunit)
    currenttime += 2 * timeunit

    index = play(1, currenttime, 4 * timeunit, timeunit, timeunit)
    currenttime += timeunit
    index = play(2, currenttime, 4 * timeunit, timeunit, timeunit)
    currenttime += 3 * timeunit
    # final
    play(0, currenttime, timeunit + duration2, timeunit, duration2)
    currenttime += timeunit

    assert currenttime == 14 * timeunit

    # prepare next section
    s = sudoku[0]
    return currenttime, s
