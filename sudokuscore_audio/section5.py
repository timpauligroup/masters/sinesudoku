import itertools

from sinesudoku.sudokuinterface import ratios, scale
from sinesudoku.sudokusynth import Synthtype


def score5(synth, time, max_amp, soundon, solver):
    """
         "Stay in Attack Position" - POINTS to POINT-LINES
         uses the noises of spaceships and laserturrets.
         glitchy.
         #nocontrol
    """
    print("SECTION 5")
    currenttime = time
    row = solver.sudoku.rows[4]
    box = solver.sudoku.boxes[4]
    column = solver.sudoku.columns[4]
    glisses = itertools.cycle(map(lambda x: ratios[int(x)], row))
    pitches = itertools.cycle(map(lambda x: scale[27 + int(x)], box))
    amps = itertools.cycle(map(lambda x: max_amp - x.normalize(), column))
    together = itertools.cycle(
        map(lambda x: x.normalize() * 0.5 + 1 / 9, itertools.chain(row, box, column)))
    overtones = 81
    repeats = 1

    def basic(length, attack, release, pitch, glis, amp, repeats, overtones):
        repeats += 3
        overtones -= 1
        s = solver.next()
        if soundon:
            assert amp <= max_amp
            synth.synthesize_sudoku(currenttime, s,
                                    Synthtype.POINT,
                                    length, attack, release,
                                    pitch, glis, amp,
                                    1, repeats, -1, overtones)
        return repeats, overtones

    def shot(length):
        return basic(length, 0, length, next(pitches),
                     next(glisses), next(amps), repeats, overtones)

    def ties(length):
        return basic(length, 1 / next(pitches), abs(next(amps)) / 81, scale[36],
                     next(glisses), max_amp, repeats, overtones)

    def wings(length):
        return basic(length, abs(next(amps)) / 81, 1 / next(pitches), scale[0],
                     next(glisses), max_amp, repeats, overtones)

    flightdur = 4
    shotdur = 0.25
    shottime = 0.5

    for i in range(2):
        for i in range(3):
            repeats, overtones = shot(shotdur)
            currenttime += shottime
        currenttime += shottime
        repeats, overtones = wings(flightdur)
        save = currenttime
        for i in range(20):
            dur = next(together)
            repeats, overtones = shot(dur)
            currenttime += dur
        currenttime = save + flightdur + shottime * 2
        repeats, overtones = ties(flightdur)
        currenttime += flightdur
    for i in range(4):
        repeats, overtones = shot(shotdur)
        currenttime += shottime
    repeats, overtones = wings(flightdur)
    save = currenttime
    for i in range(20):
        dur = next(together)
        repeats, overtones = shot(dur)
        currenttime += dur
    currenttime = save + flightdur
    for i in range(2):
        repeats, overtones = shot(shotdur)
        currenttime += shotdur
    currenttime += shottime
    repeats, overtones = ties(flightdur)
    currenttime += flightdur

    assert overtones == 3
    return currenttime
