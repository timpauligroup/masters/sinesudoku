import itertools

from sinesudoku.sudokuinterface import ratios, scale
from sinesudoku.sudokusynth import Synthtype


def score8(synth, time, max_amp, soundon, solver):
    """
         INTERLUDE II - LINE-POINT-LINE
    """
    print("SECTION 8")
    currenttime = time
    row = solver.sudoku.rows[7]
    box = solver.sudoku.boxes[7]
    column = solver.sudoku.columns[7]

    glisses = itertools.cycle(map(lambda x: ratios[int(x)], row))
    pitches = itertools.cycle(map(lambda x: scale[35] + x.normalize(), box))
    amps = itertools.cycle(map(lambda x: max_amp - x.normalize(), column))

    dur = 0.5 / solver.togo()

    while (not solver.finished()):
        s = solver.next()
        pitch = next(pitches)
        glis = next(glisses)
        amp = next(amps)
        if soundon:
            assert amp <= max_amp
            synth.synthesize_sudoku(currenttime, s,
                                    Synthtype.LINE,
                                    dur, 0, dur,
                                    pitch, glis, amp)
        currenttime += dur

    assert solver.togo() == 0
    return currenttime
