import main_audio
import main_video
from config import (audio_sco_dir, audio_secs, base, name, sec, video_secs,
                    video_sco_dir)
from sinesudoku.sudokusynth import Sudokusynth
from sinesudoku.sudokuvideo import Sudokuvideosizer


def create_seclist(number):
    seclist = [0] * 9
    seclist[number - 1] = 1
    return seclist


suf_a = '.wav'
suf_v = '.avi'


audio_base = ['scorewriter.py', 'sudoku.py', 'sudokuinterface.py',
              'sudokusines.orc', 'sudokusolver.py', 'sudokusynth.py']
audio_base = list(map(lambda x: base + x, audio_base))
audio_scores = list(map(lambda x: audio_sco_dir +
                        sec + str(x) + '.py', audio_secs))
audio_dir = Sudokusynth.build_dir


def task_main_audio():
    for s in audio_secs:
        n = sec + str(s)
        f = audio_base + audio_scores
        t = [audio_dir + n + suf_a]
        seclist = create_seclist(s)

        def a(n, seclist):
            main_audio.compose(n, seclist)

        yield {
            'name': n,
            'actions': [(a, [n, seclist])],
            'file_dep': f,
            'targets': t,
            'verbosity': 2
        }


audio_secs_wav = list(
    map(lambda x: audio_dir + sec + str(x) + suf_a, audio_secs))
final_a = [audio_dir + name + suf_a]


def task_mix_audio():
    return {
        'actions': ['python3 mix_audio.py'],
        'file_dep': ['mix_audio.py'] + audio_secs_wav,
        'targets': final_a,
        'verbosity': 2
    }


video_base = ['sudokuvideo.py']
video_base = list(map(lambda x: base + x, video_base))
video_scores = list(map(lambda x: video_sco_dir +
                        sec + str(x) + '.py', video_secs))
video_dir = Sudokuvideosizer.build_dir


def task_main_video():
    preset = 'ultrafast'
    for s in video_secs:
        n = sec + str(s)
        f = video_base + video_scores
        t = [video_dir + n + suf_v]
        seclist = create_seclist(s)

        def a(n, seclist):
            main_video.compose(n, seclist, None, preset)

        yield {
            'name': n,
            'actions': [(a, [n, seclist])],
            'file_dep': f,
            'targets': t,
            'verbosity': 2
        }


video_secs_avi = list(
    map(lambda x: video_dir + sec + str(x) + suf_v, video_secs))


def task_mix_video():
    return {
        'actions': ['python3 mix_video.py'],
        'file_dep': ['mix_video.py'] + video_secs_avi + final_a,
        'targets': [video_dir + name + '.mov'],
        'verbosity': 2
    }
