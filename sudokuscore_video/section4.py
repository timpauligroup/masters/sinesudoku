from moviepy.video.fx.all import speedx


def score4(videosizer, videopool, videoon, solver):
    print("SECTION 4 VIDEO")

    recs = 2
    pilots = [10, 7, 3, 6, 9, 2, 11, 5]
    rhy_micro = pilots + [0]
    videos = [videopool["wtfs"].pop("jeita_2")]  # dark intro
    names = ["momente_3",  # 7 3rd longest + the eye
             "momente_5",  # 3 # nazitime
             "momente_0",  # 6 # crytime eye
             "momente_2",  # 9 2nd longest mouth
             "momente_1",  # 2 eye
             "momente_4",  # 11 longest eye
             "jeita_0"]  # 5 # market
    for name in names:
        videos.append(videopool["partials"][name])

    for red, video in zip(pilots, videos):
        su = solver.next()
        v = speedx(video, final_duration=red)
        nf = red
        sp = red
        zo = (red % 3) + 1
        if videoon:
            videosizer.rasterize_sudoku(v, su, zo, sp, nf, rhy_micro, recs)

    # 1 second more because we skip sec5
    su = solver.next()
    v = videopool["wtfs"]["jeita_0"].subclip(0, 1)
    nf = 9
    sp = 9
    zo = 3
    if videoon:
        videosizer.rasterize_sudoku(v, su, zo, sp, nf, rhy_micro, recs)
