from moviepy.video.fx.all import speedx
from itertools import cycle


def score6(videosizer, videopool, videoon, solver):
    print("SECTION 6 VIDEO")

    recs = 2
    row = solver.sudoku.rows[5]
    box = solver.sudoku.boxes[5]
    column = solver.sudoku.columns[5]

    lens = [1,
            2, 1.5, 0.25, 0.25,
            2, 1.5, 0.5,
            1.5, 0.5, 0.5, 0.5, 1 / 3, 1 / 3, 1 / 3,
            3]

    videos = []
    names = ["momente_3",  # 7 3rd longest + the eye
             "momente_5",  # 3 # nazitime
             "momente_0",  # 6 # crytime eye
             "momente_2",  # 9 2nd longest mouth
             "momente_1",  # 2 eye
             "momente_4",  # 11 longest eye
             "jeita_0"]  # 5 # market
    for name in names:
        videos.append(videopool["partials"].pop(name))

    assert(len(videopool["partials"]) == 0)

    zooms = cycle(map(lambda x: 1 + 1 * x.normalize(), row))
    speeds = cycle(map(lambda x: 1 + 9 * x.normalize(), box))
    rhy_micro = list(map(lambda x: int(x), column))

    sudo = solver.next()
    vid = videopool["wtfs"].pop("jeita_0").subclip(1, 2)
    if videoon:
        videosizer.rasterize_sudoku(vid, sudo, 1, 1, 1, [1] * 9, 0)

    # drop
    for l, v, zo, sp in zip(lens, cycle(videos), zooms, speeds):
        su = solver.next()
        vi = speedx(v, final_duration=l)
        if videoon:
            videosizer.rasterize_sudoku(vi, su, zo, sp, 2, rhy_micro, recs)
