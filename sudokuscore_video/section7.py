from moviepy.video.fx.all import speedx


def score7(videosizer, videopool, videoon, solver):
    print("SECTION 7 VIDEO")

    dur = 0.5
    su = solver.next()
    v = speedx(videopool["wtfs"].pop("momente_1"), final_duration=dur)
    if videoon:
        videosizer.rasterize_sudoku(v, su, 1, 1, 1, [1] * 9, 0)
