from moviepy.video.fx.all import speedx

from sinesudoku.sudoku import Sudoku


def score1(videosizer, videopool, videoon):
    print("SECTION 1 VIDEO")

    # clean pool
    dirtynames = ["mikrophonie_5",
                  "mikrophonie_7",
                  "mikrophonie_8",
                  "momente_5",
                  "momente_6",
                  "jeita_stuff0"]
    for name in dirtynames:
        videopool["hands"].pop(name)

    # sudoku setup
    sudokus_raw = []
    sudokus_raw.append([
        [4, 9, 2, 0, 0, 0, 7, 5, 6],
        [0, 0, 0, 0, 0, 4, 8, 0, 0],
        [0, 3, 8, 0, 0, 0, 4, 2, 0],
        [6, 0, 0, 1, 8, 0, 5, 0, 0],
        [0, 0, 0, 0, 6, 0, 0, 0, 0],
        [0, 0, 7, 0, 9, 5, 0, 0, 1],
        [0, 6, 1, 0, 0, 0, 2, 7, 0],
        [0, 0, 3, 8, 0, 0, 0, 0, 0],
        [2, 4, 5, 0, 0, 0, 9, 3, 8]])

    sudokus_raw.append([
        [3, 0, 0, 0, 2, 9, 0, 0, 7],
        [0, 4, 0, 0, 1, 0, 0, 2, 3],
        [0, 0, 0, 0, 0, 3, 9, 0, 5],
        [0, 0, 3, 0, 8, 0, 0, 0, 9],
        [0, 2, 8, 3, 0, 4, 5, 6, 0],
        [5, 0, 0, 0, 6, 0, 2, 0, 0],
        [8, 0, 5, 7, 0, 0, 0, 0, 0],
        [2, 7, 0, 0, 5, 0, 0, 8, 0],
        [4, 0, 0, 9, 3, 0, 0, 0, 2]])

    sudokus_raw.append([
        [0, 9, 7, 0, 0, 1, 5, 0, 0],
        [5, 0, 0, 6, 7, 0, 8, 0, 0],
        [0, 0, 0, 0, 4, 0, 0, 3, 0],
        [0, 5, 0, 0, 0, 0, 2, 0, 0],
        [8, 3, 0, 0, 0, 0, 0, 5, 7],
        [0, 0, 6, 0, 0, 0, 0, 1, 0],
        [0, 2, 0, 0, 9, 0, 0, 0, 0],
        [0, 0, 5, 0, 1, 4, 0, 0, 8],
        [0, 0, 1, 3, 0, 0, 9, 2, 0]])

    sudokus_raw.append([
        [0, 1, 7, 2, 0, 0, 0, 0, 0],
        [0, 9, 2, 3, 7, 8, 0, 0, 5],
        [4, 8, 0, 6, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 6, 9, 2],
        [0, 0, 0, 0, 2, 0, 0, 0, 0],
        [2, 7, 4, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 3, 0, 6, 7],
        [1, 0, 0, 8, 5, 7, 9, 2, 0],
        [0, 0, 0, 0, 0, 2, 5, 3, 0]])

    sudokus_raw.append([
        [0, 8, 1, 7, 0, 4, 0, 0, 0],
        [7, 0, 0, 0, 0, 0, 0, 0, 3],
        [0, 0, 0, 5, 2, 0, 0, 9, 0],
        [4, 1, 3, 0, 0, 0, 0, 0, 0],
        [0, 0, 9, 0, 0, 0, 8, 0, 0],
        [0, 0, 0, 0, 0, 0, 3, 6, 9],
        [0, 7, 0, 0, 8, 5, 0, 0, 0],
        [5, 0, 0, 0, 0, 0, 0, 0, 8],
        [0, 0, 0, 1, 0, 9, 7, 5, 0]])

    sudokus = []
    for raw in sudokus_raw:
        sudokus.append(Sudoku(raw, 9))

    row = sudokus[0].rows[0]
    box = sudokus[0].boxes[0]
    column = sudokus[0].columns[0]

    zoom_factors = [3] * 9
    speed_factors = map(lambda x: 3 + 9 * x.normalize(), box)
    nframes = map(lambda x: 3 + 9 * x.normalize(), row)
    rhy_micro = list(map(lambda x: int(x), column))

    timeunit = 4
    recs = 1

    videos = [videopool["wtfs"].pop("jeita_1")]  # dark intro
    filenames = ["momente_drum4",  # drum intro
                 "mikrophonie_mikro1",  # mikro expo
                 "mikrophonie_switch2",
                 "jeita_mixer7",
                 "jeita_radio4",
                 "momente_real2",  # fixed because pre final
                 "mikrophonie_mikro10"]  # fixed because final
    for name in filenames:
        videos.append(videopool["hands"].pop(name))

    sudoku_stream = [sudokus[i] for i in [0, 2, 1, 4, 3, 1, 2, 0]]
    rhythm_macro = [i * timeunit for i in [1, 3, 1, 2, 1, 2, 1, 3]]
    assert(sum(rhythm_macro) == 14 * timeunit)

    def play(clip, dur, su, zo, sp, nf):
        if videoon:
            clip = speedx(clip, final_duration=dur)
            videosizer.rasterize_sudoku(clip, su, zo, sp, nf, rhy_micro, recs)

    loop = zip(videos, rhythm_macro, sudoku_stream,
               zoom_factors, speed_factors, nframes)

    for vid, dur, su, zo, sp, nf in loop:
        play(vid, dur, su, zo, sp, nf)

    # prepare next section
    s = sudokus[0]
    return s
