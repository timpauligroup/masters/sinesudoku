from itertools import chain, cycle
from math import floor

from moviepy.video.fx.all import speedx


def score3(videosizer, videopool, videoon, solver):
    print("SECTION 3 VIDEO")
    row = solver.sudoku.rows[2]
    box = solver.sudoku.boxes[2]
    column = solver.sudoku.columns[2]

    timestep = 4
    recs = 1

    assert(len(videopool["hands"]) == 8)

    videos = []
    filenames = ["mikrophonie_drum6",  # drum intro
                 "mikrophonie_mikro0",  # mikro expo
                 "jeita_stuff5",
                 "jeita_switch3",
                 "jeita_mixer8",
                 "momente_real3",  # fixed because pre final
                 "mikrophonie_mikro3",  # fixed because final 1
                 "momente_god0"]  # fixed because final 2
    for name in filenames:
        videos.append(videopool["hands"].pop(name))

    assert(len(videopool["hands"]) == 0)

    def single(idx):
        result = [0] * 9
        result[idx] = 1
        return result

    rhy_micros = chain(row, box, column)
    rhy_micros = filter(lambda x: x != 0, rhy_micros)
    rhy_micros = map(lambda x: int(x) - 1, rhy_micros)
    rhy_micros = [single(r) for r in rhy_micros]
    rhy_micros = cycle(rhy_micros)
    zooms = map(lambda x: 1 + 3 * x.normalize(), box)
    speeds = map(lambda x: 1 + 3 * x.normalize(), row)
    nfs = map(lambda x: 3 + 3 * x.normalize(), column)

    def synthesize(videos, shutter, zo, sp, nf):
        number_shuts_total = timestep / shutter
        last_add = (number_shuts_total - floor(number_shuts_total)) * shutter
        number_shuts_total = floor(number_shuts_total)
        assert(number_shuts_total >= len(videos))

        durs = number_shuts_total * [shutter]
        durs[len(durs) - 1] += last_add
        assert(round(sum(durs), 9) == timestep)

        for video, dur, rhy_micro in zip(cycle(videos), durs, rhy_micros):
            su = solver.next()
            v = speedx(video, final_duration=dur)
            if videoon:
                videosizer.rasterize_sudoku(v, su, zo, sp, nf, rhy_micro, recs)

    fps = videos[0].fps
    shutters = [4, 2, 1, 0.5, 9 / fps, 9 / fps, 2 / fps, 1 / fps]

    vids_evol = [videos[:i] for i in [1, 2, 4, 6, 7, 8, 8, 8]]
    loop = zip(vids_evol, shutters, zooms, speeds, nfs)

    for v, shutter, zo, sp, nf in loop:
        synthesize(v, shutter, zo, sp, nf)
