from moviepy.video.fx.all import speedx


def score8(videosizer, videopool, videoon, solver):
    print("SECTION 8 VIDEO")

    dur = 0.5
    su = solver.next()
    v = speedx(videopool["wtfs"].pop("momente_0"), final_duration=dur)
    if videoon:
        videosizer.rasterize_sudoku(v, su, 1, 1, 1, [1] * 9, 0)

    while (not solver.finished()):
        solver.next()

    assert(solver.togo() == 0)
    assert(len(videopool["wtfs"]) == 0)
